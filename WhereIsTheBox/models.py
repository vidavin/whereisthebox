from django.db import models

# Create your models here.

class Home(models.Model):
    Name = models.CharField(max_length=200)
    Owner = models.ForeignKey('User', on_delete=models.CASCADE)

    def __str__(self) -> str:
        return super().__str__()
    
    def toJson(self):
        return {
            'Name': self.Name,
            'Owner': self.Owner.toJson()
        }

# Class Box contains a Name (str), a tag (str), an Owner (User), a StorageBox (Box) and a StoredBox (Box)
class Box(models.Model):
    Name = models.CharField(max_length=200)
    Tag = models.CharField(max_length=200)
    Owner = models.ForeignKey('User', on_delete=models.CASCADE)
    Home = models.ForeignKey('Home', on_delete=models.CASCADE, null=True)
    StorageBox = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name='%(class)s_storagebox')    # Box Fils
    StoredBox = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name='%(class)s_storedbox')      # Box Père
    

    def toJson(self):
        return {
            'Id': self.id,
            'Name': self.Name,
            'Tag': self.Tag,
            'Owner': self.Owner.toJson(),
            'Home': self.Home.toJson() if self.Home != None else None,
            'StorageBox': self.StorageBox.Name if self.StorageBox != None else None,
            'StoredBox': self.StoredBox.Name if self.StoredBox != None else None
        }

    def __str__(self):
        return "Name: " + self.Name + " Owner: " + self.Owner.__str__() + " Tag: " +  self.Tag + "StorageBox: " + self.StorageBox.__str__() + " StoredBox: " + self.StoredBox.__str__()
    
    def setStorageBox(self, box):
        self.StorageBox = box
        self.save()

    def setStoredBox(self, box):
        self.StoredBox = box
        self.save()

    # Return the list of Box convert in Json
    def getListBox(self):
        listBox = []
        for box in Box.objects.all():
            listBox.append(box.toJson())
        return listBox
        
    
class User(models.Model):
    Name = models.CharField(max_length=200)
    Email = models.CharField(max_length=200)
    
    def __str__(self):
        return self.Name + " " + self.Email
    
    def toJson(self):  
        return {
            'Name': self.Name,
            'Email': self.Email
        }


# Je dois faire les classes Object ici