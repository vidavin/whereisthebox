from django.shortcuts import render
from django.http import HttpResponse
from django.http import Http404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Box, User


def index(request):
    return JsonResponse({'foo': 'bar'})

# Return a Json response with the list of boxes stored in the database.
def getAllBoxes(request):
    listBox = []
    for box in Box.objects.all():
        listBox.append(box.toJson())
    return JsonResponse({'listBox': listBox})

def getBoxName(request, boxName):
    listBox = []
    for box in Box.objects.all():
        if box.Name.startswith(boxName): 
            listBox.append(box.toJson())
    if len(listBox) == 0:
        raise Http404("Box does not exist")
    return JsonResponse({'listBox': listBox})

def deleteBox(request, boxId):
    try:
        box = Box.objects.get(pk=boxId)
        box.delete()
    except Box.DoesNotExist:
        raise Http404("Box does not exist")
    return JsonResponse({'box': box.toJson()})

# Create a new box from a Json request
@csrf_exempt
def createBox(request):
    #Own1 = User(Name=request.POST.get('UserName'), Email=request.POST.get('UserEmail'))
    #Own1.save()
    #box.Name = request.POST.get('BoxName')
    #box.Tag = request.POST.get('Tag')
    #box.Owner = request.POST.get('Owner')
    #box.Home = request.POST.get('Home')
    #box.StorageBox = request.POST.get('StorageBox')
    #box.StoredBox = request.POST.get('StoredBox')    
    #box.save()
    return HttpResponse(status=501)

# TODO Refaire WhereIsBox pour que ce soit récursif
def whereIsBox(request, boxId):
    listBox = []
    try:
        box = Box.objects.get(pk=boxId)
        listBox.append(box.toJson())
    except Box.DoesNotExist:
        raise Http404("Box does not exist")
    while(box.StoredBox != None):
        box = box.StoredBox
        listBox.append(box.toJson())
    return JsonResponse({'listBox': listBox})


def getBoxId(request, box_id):
    try:
        box = Box.objects.get(pk=box_id)
    except Box.DoesNotExist:
        raise Http404("Box does not exist")
    print(box.id)
    return JsonResponse({'box': box.toJson()})


# Create your views here.


def detail(request, box_id):
    return HttpResponse("You're looking at box %s." % box_id)