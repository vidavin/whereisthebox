from django.urls import path

from . import views 

urlpatterns = [
    path('', views.index, name='index'),
    path('getAllBoxes', views.getAllBoxes, name='getAllBoxes'),
    path('getBoxId/<int:box_id>', views.getBoxId, name='getBox'),
    path('getBoxName/<str:boxName>', views.getBoxName, name='getBoxName'),
    path('whereIsBox/<int:boxId>', views.whereIsBox, name='whereIsBox'),
    path('postNewBox', views.createBox, name='createBox'),
    path('deleteBox/<int:boxId>', views.deleteBox, name='deleteBox'),
]